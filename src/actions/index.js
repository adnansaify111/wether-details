import axios from 'axios'

const API_KEY = '1d601fb31abca0419b36cd40496c5d47'
const ROOT_URL =`http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`

export const FETCH_WEATHER = 'FETCH_WEATHER'

/*export function fetchWeather(city){
    console.log('i am in action')
    const url =`${ROOT_URL}&q=${city},IN`
    const request = axios.get(url);
    console.log('i have fetched weather details for you',request)
    return{
        type:FETCH_WEATHER,
        payload:request
    }
}*/

export function fetchWeather(city) {
    return function(dispatch) {
    dispatch({type: FETCH_WEATHER});
    const url =`${ROOT_URL}&q=${city},US`
      
      axios.get(url)
        .then((response) => {
          dispatch({type: "FETCH_WEATHER_SUCCESS", payload: response})
        })
        .catch((err) => {
          dispatch({type: "FETCH_WEATHER_FAILED", payload: err})
        })
    }
  }
  