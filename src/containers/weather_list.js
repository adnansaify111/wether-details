import React,{Component} from 'react'
import { connect } from 'react-redux';
import Chart from '../components/weather_chart'
import GoogleMap from '../components/google_map'


class WeatherList extends Component{
    constructor(props){
        super();
        
    }
    renderWeather(cityData){
        if(!cityData){
            return<div>loading..</div>
        }
        const names = cityData.city.name
        const temps=cityData.list.map(weather => weather.main.temp)
        const humidity=cityData.list.map(weather => weather.main.humidity)
        const pressures=cityData.list.map(weather => weather.main.pressure)
        const {lat,lon}= cityData.city.coord
        return(
            <tr key={names}>
                {/* {<td><GoogleMap lat={lat} lon={lon} /></td> */}
                <td>
                    <Chart data={temps} color='blue' units='K'/>
                </td>
                <td>
                    <Chart data={humidity} color='blue' units='%'/>
                </td>
                <td>
                     <Chart data={pressures} color='blue' units='hpa'/>
                </td>
            </tr>
        )
    }
    render(){
        console.log(this.props.weather)
        return(
            <table className='table table-hover'>
                <thead>
                    <tr>
                        <th>City</th>
                        <th>Temperature(K)</th>
                        <th>Humidity(%)</th>
                        <th>Pressure(hpa)</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.weather.map(this.renderWeather)}
                </tbody>
            </table>
        );
    }
}

function mapStateToProps(state){
    return {
        weather:state.weather.data,
        status:state.weather
    }
}

export default connect(mapStateToProps)(WeatherList);